#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
# Copyright (C) 2021  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


# Use an official Dgraph runtime as a parent image
FROM dgraph/dgraph AS go-build
WORKDIR /usr/local/bin/go-build

# Copy the current directory contents into the container at working directory
COPY . /usr/local/bin/go-build

# Install go and build cidr-plugin.go
RUN apt-get update \
    && apt-get install -y build-essential \
    && export GO_VERSION=`dgraph version | grep 'go.*' | cut -d" " -f10-` \
    && curl -O https://dl.google.com/go/$GO_VERSION.linux-amd64.tar.gz 1>/dev/null \
    && tar xvf $GO_VERSION.linux-amd64.tar.gz \
    && mv go /usr/local \
    && export GOPATH=$HOME/work \
    && export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin \
    && go build -ldflags="-s -w" -buildmode=plugin -o cidr-plugin.so ./dgraph-plugins/cidr-plugin.go

# Copy the created plugin and start with clean images
FROM dgraph/dgraph AS main-build
WORKDIR /usr/local/bin/granef/
COPY --from=go-build /usr/local/bin/go-build /usr/local/bin/granef

# Install Python
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --yes python3-minimal python3-pip \
    && pip3 install --trusted-host pypi.python.org --no-cache-dir -r requirements.txt \
    && apt-get purge --yes python3-pip \
    && apt-get autoremove --yes

# Run communication-handler.py with arguments when container launches (CMD if there are no arguments) 
ENTRYPOINT ["python3", "communication_handler.py"]
