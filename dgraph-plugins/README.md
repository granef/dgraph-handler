# Dgraph Plugins

Repository contains plugins for graph database Dgraph written in Go. In the analytical process they are loaded 
by Dgraph data handling module.

They have to be build with the same version of Go as the currently running Dgraph (this is handled in the corresponding Dockerfiles). 

If build separately, use this command:

```
go build -buildmode=plugin -o <plugin_name>.so <plugin_name>.go
```

When running `dgraph alpha` or `dgraph bulk` commands use the `--custom_tokenizers` argument as follows:

```
--custom_tokenizers=<plugin_name1>.so,<plugin_name2>.so
```

*(Also if a new index plugin is being implemented, don't forget to update `zeek-dgraph.schema` file (or directly the
[transformation module](https://gitlab.ics.muni.cz/granef/transformation-zeek-dgraph)).)*

### Usage of data indexed by the plugins

#### `cidr-plugin.go` plugin

The default Dgraph settings allow querying the IP address either for a complete match or using regular expressions. Nevertheless, querying network data based on CIDR ranges is also desired in traditional network traffic analysis. The `cidr-plugin.go` plugin enables such a feature. Below are examples of concrete queries that facilitate the usage of such CIDR filter.

Get all local connections (CIDR ranges: 10.0.0.0/8; 172.16.0.0/12; 192.168.0.0/16):

```
{ 
 getLocalConnections(func: allof(host.ip, cidr, "192.168.0.0/16")) { 
    name : host.ip 
  
    host.originated {
      expand(Connection)
    }
  
    host.responded {
      expand(Connection)
    }
  } 
}
```

Get simple statistics about hosts from a specified CIDR range:

```
{ 
 getStatistics(func: allof(host.ip, cidr, "192.168.0.0/16")) { 
    name : host.ip 
    
    host.hostname {
      hostname.name 
      hostname.type 
    }
  
    host.user_agent {
      user_agent.name 
      user_agent.type
    } 
    
    obtained_file_count : count(host.obtained) 
    provided_file_count : count(host.provided)
    communicated_count : count(host.communicated)
    originated_count : count(host.originated)
    responded_count : count(host.responded)
    x509_count : count(host.x509)
  } 
}
```
