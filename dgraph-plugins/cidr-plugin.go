/*
Granef -- graph-based network forensics toolkit
Copyright (C) 2020-2021  Milan Cermak, Institute of Computer Science of Masaryk University
Copyright (C) 2020-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


package main

import (
        "net"
        "strings"
)

func Tokenizer() interface{} { return CIDRTokenizer{} }

type CIDRTokenizer struct{}

func (CIDRTokenizer) Name() string     { return "cidr" }
func (CIDRTokenizer) Type() string     { return "string" }
func (CIDRTokenizer) Identifier() byte { return 0xff }

func (t CIDRTokenizer) Tokens(value interface{}) ([]string, error) {
        input := value.(string)

	// _0 -> : because IPv6 adresses are saved with _0 instead of : in 
	// the database (Data Transformation)
	if strings.Contains(input, "_0") {
                input = strings.ReplaceAll(input, "_0", ":")
        }

        if  !strings.Contains(input, "/") {
                if strings.Contains(input, ":") {
                        input = input + "/128"
                } else {
                        input = input + "/32"
                }
        }

        _, ipnet, err := net.ParseCIDR(input)

        if err != nil {
                return nil, err
        }

        ones, bits := ipnet.Mask.Size()
        var toks []string

        for i := ones; i >= 0; i-- {
                m := net.CIDRMask(i, bits)
                tok := net.IPNet{
                        IP:   ipnet.IP.Mask(m),
                        Mask: m,
                }

                toks = append(toks, tok.String())
        }

        return toks, nil
}
