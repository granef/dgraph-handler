#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#
# Granef -- graph-based network forensics toolkit
# Copyright (C) 2019-2021  Denisa Sramkova, Institute of Computer Science of Masaryk University
# Copyright (C) 2021-2022  Milan Cermak, Institute of Computer Science of Masaryk University
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#


"""
Script:
In INDEXING MODE:
1. Loads RDF and schema files from a local directory.
2. Applies indexing (Dgraph Zero).
3. The result is saved to a local directory called 'indexing'.

IN DGRAPH ZERO MODE:
1. Runs Dgraph Zero and handles its arguments

IN DGRAPH ALPHA MODE:
1. Loads indexed data from a local directory.
3. Uploads them to database.

Usage:
* Indexing
    $ ./communication_handler.py -mm indexing -i <input_dir_path> -o <output_dir_path> -ms <map_shards>
      -rs <reduce_shards> -z <zero> -c <custom_tokenizers> -b <bulk_args>

* Zero
    $ ./communication_handler.py -mm zero -my <ip:port> --bindall <boolean>

* Alpha (obtain data via local directory):
    $ communication_handler.py -mm alpha -i <input_dir_path> -o <output_dir_path> --zero=<ip:port>
      -my=<ip:port> -ap 0/p/ -of <alpha_o> -aa <alpha_args>
"""

import sys
import re
import fnmatch
import os
import signal
import shutil
import argparse
import ipaddress
import subprocess
import multiprocessing
import logging, coloredlogs


def fix_permissions(path):
    os.chmod(path, 0o0777)
    for subdir, _, files in os.walk(path):
        os.chmod(subdir, 0o0777)
        for file in files:
            file_path = os.path.join(subdir, file)
            os.chmod(file_path, 0o0666)


def is_integer(val):
    """
        Checks if input val is an integer.

        :return: True if val is a integer
    """
    try:
        int(val)
        return True
    except ValueError:
        return False


def is_ip_address(string):
    """
        Checks if string is a valid IP address.

        :return: True if string is a valid IP address
    """
    try:
        ipaddress.ip_address(string)
        return True
    except ValueError:
        return False


def is_hostname(string):
    """
        Checks if string is a valid hostname.
        Source: https://stackoverflow.com/questions/2532053/validate-a-hostname-string

        :return: True if string is a valid hostname
    """
    if len(string) > 255:
        return False
    if string[-1] == '.':
        string = string[:-1]
    allowed = re.compile('(?!-)[A-Z\d-]{1,63}(?<!-)$', re.IGNORECASE)
    return all(allowed.match(x) for x in string.split('.'))


def is_ip_address_or_hostname(string):
    """
        Checks if string is a valid ip_address or hostname.

        :return: string if True
    """
    return is_ip_address(string) or is_hostname(string)


def is_port(val):
    """
        Check if the given val is a valid port number.

        :param val: port number expected to be in range [0, 65536]
        :return: True if val is in valid format
    """
    if is_integer(val) and 0 < int(val) < 65536:
        return val
    raise argparse.ArgumentTypeError('Given argument is not a valid port: ' + val)


def ip_and_port(string):
    """
        Check if the given string is in valid format of 'ip:port' or 'hostname:port'.

        :param string: string that contains a : (e.g. '121.11.10.9:1234')
        :return: True if the string is in valid format
    """
    parts = string.split(":")
    if len(parts) == 2 and is_ip_address_or_hostname(parts[0]) and is_port(parts[1]):
        return string
    raise argparse.ArgumentTypeError('Given string is not a valid IP address or a hostname: ' + string)


def check_file_availability(input_dir, alpha_p):
    file_dir = input_dir + '/' + alpha_p
    if os.path.isdir(file_dir):
        return file_dir
    else:
        logger.error('Input path "' + file_dir + '" is not a valid directory')
        return ''


def get_alpha_input(input_path, alpha_p):
    # if "0/p/" given as alpha_p, check if the input dir path contains "0/p/"
    input_dir_path = check_file_availability(input_path, alpha_p)
    logger.debug('Input directory "' + input_dir_path + '" will be used to search for input indexed files')
    return input_dir_path


def output_dir_already_contains_output_file(output_dir_path, output_file_extension):
    """
        Checks if an output file ends with a given extension in the output directory.

        :param output_dir_path: directory that is searched
        :param output_file_extension: specifies what file(s) to look for based on their ending(s)
        :return: True if all files that end with given extensions exist, False otherwise
    """
    if os.path.isdir(output_dir_path):
        for file in os.listdir(output_dir_path):
            if file.endswith(output_file_extension):
                return True

    return False


def get_indexing_input_paths(dir_path):
    """
        Check file rdf, schema files availability in dir_path directory.

        :param dir_path: path to input directory
        :return: list of local input file paths
    """
    rdf_path = ''
    schema_path = ''

    for folder_name, _, filenames in os.walk(dir_path):
        for file in filenames:
            if file.endswith('.rdf'):
                rdf_path += str(os.path.join(folder_name, file)) + ","
            elif file.endswith('.schema'):
                schema_path += str((os.path.join(folder_name, file))) + ","

    if rdf_path and schema_path:
        logger.debug('Found input RDF files: ' + rdf_path[:-1] + '; and Schema files: ' + schema_path[:-1])

    return rdf_path[:-1], schema_path[:-1]


def get_indexing_inputs(dir_path):
    """
        Check availability/ download input file. Extract it and check if it contains rdf and schema files.

        :param dir_path: path to the directory where the inputs should be located
        :return: list (path to rdf file, path to schema file)
    """
    if not os.path.isdir(dir_path):
        logger.error('Given input path "' + dir_path + '" is not a directory')
        return '', ''
    else:
        logger.debug('Looking for input files in directory: ' + dir_path)
        input_files = get_indexing_input_paths(dir_path)
    return input_files


def generate_indexing_command(args, input_rdf, input_schema):
    other_args = ''
    reduce_shards = str(args.reduce_shards)
    custom_tokenizers = str(args.custom_tokenizers)
    bulk_agrs = args.bulk_args
    if reduce_shards:
        other_args += '--reduce_shards=' + str(reduce_shards)
    if custom_tokenizers:
        other_args += ' --custom_tokenizers=' + custom_tokenizers.strip()
    if bulk_agrs:
        other_args += ' ' + bulk_agrs

    return 'dgraph bulk -f ' + input_rdf + ' -s ' + input_schema + ' --map_shards=' + str(args.map_shards) + ' -z ' + \
           args.zero + ' --replace_out --out ' + output + ' ' + other_args


def merge_schema_files(schema_files_path):
    schema_files = schema_files_path.split(",")
    schema = ""
    # Load the first schema file
    with open(schema_files[0], "r") as init_schema_file:
        schema += init_schema_file.read() + "\n"

    # Append other schema files withoud duplicites
    for additional_schema_file in schema_files[1:]:
        with open(additional_schema_file, "r") as schema_file:
            for line in schema_file:
                # Check if its the attribute definition and do not append existing ones
                line_split = line.split(":")
                if len(line_split) > 1 and (line_split[0] in schema):
                    continue
                schema += line

    # Write the merged schema to temporary directory
    temp_schema_path = "/tmp/merged.schema"
    with open(temp_schema_path, "w") as output_schema:
        output_schema.write(schema)
    return temp_schema_path


def generate_alpha_command(args, data_input):
    other_args = ''
    arg_other = getattr(args, 'alpha_args')
    custom_tokenizers = args.custom_tokenizers
    arg_o = int(getattr(args, 'alpha_o'))
    if arg_o > 0:
        other_args += '-o=' + str(arg_o)
    if custom_tokenizers:
        other_args += ' --custom_tokenizers=' + custom_tokenizers.strip()
    if arg_other:
        other_args += ' ' + arg_other

    # running Dgraph Alpha uploads the data (from directory output_dir + getattr(args, 'alpha_p')) to graph db
    return ' dgraph alpha --zero ' + args.zero + ' --my ' + args.my + \
           ' -p ' + data_input + ' ' + other_args


def define_parsed_arguments():
    """
        Add arguments to ArgumentParser (argparse) module instance.

        :return: Parsed arguments
    """
    parser = argparse.ArgumentParser()

    # script mode has to be chosen
    subparser = parser.add_subparsers(dest='module_mode', help='Module mode (one has to be chosen)')
    subparser.required = True
    indexing = subparser.add_parser('indexing')
    zero = subparser.add_parser('zero')
    alpha = subparser.add_parser('alpha')
    # https://towardsdatascience.com/a-simple-guide-to-command-line-arguments-with-argparse-6824c30ab1c3

    parser.add_argument('-i', '--input', help='Input data directory path', default='/data/', type=str)
    parser.add_argument('-o', '--output', help='Output data directory path', default='granef-indexing/', type=str)
    parser.add_argument('-m', '--mounted', help='Mounted data directory path', default='/data/', type=str)
    parser.add_argument('-l', '--log', choices=['debug', 'info', 'warning', 'error', 'critical'], help='Log level',
                        default='INFO')

    # dgraph bulk command arguments (indexing)
    indexing.add_argument('-f', '--force', help='Forces to overwrite any files that are already present in the '
                                                'output data directory', action='store_true')
    indexing.add_argument('-ms', '--map_shards', help='Bulk command "--map_shards" argument value', default=2, type=int)
    indexing.add_argument('-rs', '--reduce_shards', help='Bulk command "--reduce_shards" argument value', default=1,
                          type=int)
    indexing.add_argument('-z', '--zero', help='Bulk command "--zero" argument value', default='localhost:5080',
                          type=str)
    indexing.add_argument('-c', '--custom_tokenizers', help='Bulk command "--custom_tokenizers" argument value',
                          default='cidr-plugin.so', type=str)
    indexing.add_argument('-b', '--bulk_args', help='Other bulk command arguments but in format "--arg value"',
                          default='', type=str)

    # dgraph zero command arguments
    zero.add_argument('-my', "--my", help='Zero command "--my" argument value', default='localhost:5080',
                      type=ip_and_port)
    zero.add_argument('-ba', '--bindall', help='Zero command "--bindall" argument value', default=True, type=bool)

    # dgraph alpha command arguments
    alpha.add_argument('-z', '--zero', help='Alpha command "--zero" argument value', default='zero:5080',
                       type=ip_and_port)
    alpha.add_argument('-my', '--my', help='Alpha command "--my" argument value', default='localhost:7080',
                       type=ip_and_port)
    alpha.add_argument('-c', '--custom_tokenizers', help='Alpha command "--custom_tokenizers" argument value',
                       default='cidr-plugin.so', type=str)
    alpha.add_argument('-ap', '--alpha_p', help='Alpha command "-p" argument value (data directory specifier)',
                       default='p/', type=str)
    alpha.add_argument('-of', '--alpha_o', help='Alpha command "-o" argument value (port offset for another Alpha)',
                       default=0, type=int)
    alpha.add_argument('-aa', '--alpha_args', help='Other alpha command arguments but in format "--arg value"',
                       default='', type=str)

    return parser.parse_args()


if __name__ == '__main__':
    args = define_parsed_arguments()
    mode = args.module_mode

    logger = logging.getLogger(mode.capitalize())
    coloredlogs.install(level=getattr(logging, args.log.upper()),
                        fmt='%(asctime)s %(name)s [%(levelname)s]: %(message)s')

    input = args.input
    output = args.mounted + '/' + args.output
    run_command = True

    if args.input.replace('/', '') != args.mounted.replace('/', ''):
        input = args.mounted + '/' + args.input
    logger.debug('Arguments set to: input = ' + input + ', output = ' + output)
    command = ''

    # --------------
    # indexing mode
    if mode == 'indexing':
        logger.debug('Dgraph Handling mode: INDEXING')

        # check input rdf and schema files availability
        input_rdf_files, input_schema_files = get_indexing_inputs(input)

        if input_rdf_files and input_schema_files:
            merged_schema_path = merge_schema_files(input_schema_files)
            command = generate_indexing_command(args, input_rdf_files, merged_schema_path)
        else:
            logger.warning('No input files found, no indexed files generated')
            run_command = False

    # --------------
    # zero mode
    if mode == 'zero':
        my = args.my
        zero = args.my
        logger.debug('Dgraph Handling mode: ZERO')
        bindall = str(args.bindall)
        command = ' dgraph zero --my=' + my + ' --bindall=' + bindall

    # --------------
    # alpha mode
    if mode == 'alpha':
        logger.debug('Dgraph Handling mode: ALPHA')
        alpha_p = str(getattr(args, 'alpha_o')) + '/' + getattr(args, 'alpha_p')

        data_input = get_alpha_input(input, alpha_p)

        if data_input and os.path.isdir(data_input):
            command = generate_alpha_command(args, data_input)
        else:
            logger.error('Dgraph Alpha did not start')
            run_command = False

    if run_command:
        try:
            if mode == 'indexing':
                # Create output directory if not exists
                if not os.path.isdir(output):
                    os.mkdir(output)

                if output_dir_already_contains_output_file(output + '/0/p/', '.sst') and not args.force:
                    # if the output directory does already exist, it is checked whether it does
                    # not already contain the output files (then the user is warned)
                    logger.warning(
                        'The output directory (' + output + ') seems to already contain the corresponding output '
                        'file(s). If you wish to run this Module anyway, run it with the -f/--force option to '
                        'overwrite the current content of the output directory.')
                    exit()

                # run processes in parallel
                zero_process = subprocess.Popen('exec dgraph zero', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                bulk_process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                # TODO: simulate an error by dgraph bulk and check if dgraph logs still overwrite everything in workdir

                # wait for completion
                outs, errs = bulk_process.communicate()
                returncode = bulk_process.returncode

                if returncode == 0:
                    logger.debug('Output files successfully generated to "' + output + '"')
                    fix_permissions(output)
                else:
                    logging.error('Command "' + command + '" failed with return code: ' + str(returncode))
                    logging.debug('Command STDOUT:\\n' + outs.decode('utf-8').replace('\n', '\\n'))
                    logging.debug('Command STDERR:\\n' + errs.decode('utf-8').replace('\n', '\\n'))
                    # Remove output directory
                    shutil.rmtree(output)
                
                # Kill Dgraph zero process
                zero_process.send_signal(signal.SIGTERM)
            else:
                logger.debug('Running command: ' + command)
                subprocess.run(command, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        except subprocess.CalledProcessError as e:
            logger.error('(CalledProcessError) When running command:\nCommand = ' + str(e.cmd) + ',\nReturncode = ' +
                          str(e.returncode) + ',\nOutput = ' + str(e.output))
        except Exception as e:
            logger.error('When running defined command:' + str(e))
